package cat.itb.testingaplication;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isRoot;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    public String USER_TO_BE_TYPED = "user";
    public String PASS_TO_BE_TYPED = "password1";


    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule
            = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void check_elements_display_correctly() {

        onView(withId(R.id.textViewTitle)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));

    }

    @Test
    public void check_String_title_and_button_text() {

        onView(withId(R.id.textViewTitle)).check(matches(withText("Main Activity Title")));
        onView(withId(R.id.button)).check(matches(withText("Next")));
    }

    @Test
    public void check_if_button_text_change() {

        onView(withId(R.id.button)).check(matches(isClickable()));
        onView(withId(R.id.button)).perform(click()).check(matches(withText("Back")));
    }

    @Test
    public void login_form_behaviour(){

        onView(withId(R.id.editTextTextUsername)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.editTextTextPassword)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());

        onView(withId(R.id.button)).perform(click()).check(matches(withText("Logged")));
    }

    @Test
    public void check_change_activity() {
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.ActivityWelcome)).check(matches(isDisplayed()));
    }


    @Test
    public void check_onClick_returns(){
        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.ActivityWelcome)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonWelcome)).perform(click());
        onView(withId(R.id.MainActivityId)).check(matches(isDisplayed()));

        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.ActivityWelcome)).check(matches(isDisplayed()));
        onView(isRoot()).perform(pressBack());
        onView(withId(R.id.MainActivityId)).check(matches(isDisplayed()));
    }

    @Test
    public void large_test_function(){
        onView(withId(R.id.editTextTextUsername)).perform(typeText(USER_TO_BE_TYPED)).perform(closeSoftKeyboard());
        onView(withId(R.id.editTextTextPassword)).perform(typeText(PASS_TO_BE_TYPED)).perform(closeSoftKeyboard());

        onView(withId(R.id.button)).perform(click());
        onView(withId(R.id.ActivityWelcome)).check(matches(isDisplayed()));

        onView(withId(R.id.textViewWelcomeTitle)).check(matches(withText("Welcome back "+USER_TO_BE_TYPED)));

        onView(isRoot()).perform(pressBack());
        onView(withId(R.id.MainActivityId)).check(matches(isDisplayed()));

        onView(withId(R.id.editTextTextUsername)).check(matches(withText("")));
        onView(withId(R.id.editTextTextPassword)).check(matches(withText("")));
    }
}
