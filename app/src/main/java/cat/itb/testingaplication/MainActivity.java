package cat.itb.testingaplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    EditText text, text2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        text = findViewById(R.id.editTextTextUsername);
        text2 = findViewById(R.id.editTextTextPassword);

        text.setText("");
        text2.setText("");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button.setText("Back");
                button.setText("Logged");
                Intent i = new Intent(getApplication(), WelcomeActivity.class);
                i.putExtra("name",text.getText().toString());
                startActivity(i);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        text.setText("");
        text2.setText("");
    }
}