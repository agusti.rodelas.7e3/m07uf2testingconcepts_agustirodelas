package cat.itb.testingaplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {

    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        Intent intent = getIntent();

        textView = findViewById(R.id.textViewWelcomeTitle);

        textView.setText("Welcome back "+ intent.getStringExtra("name"));

        findViewById(R.id.buttonWelcome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent newI = new Intent(getApplication(), MainActivity.class);
                startActivity(newI);
            }
        });
    }
}